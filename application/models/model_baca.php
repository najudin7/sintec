<?php
class model_baca extends ci_model {
	function tampil() {
		$query=$this->db->get('artikel');
		if($query->num_rows()>0) 
		{
			return $query->result();
		}
		else
		{
			return array();
		}
	}

	function per_id($id)
	{
		$this->db->where('id_artikel',$id);
		$query=$this->db->get('artikel');
		return $query->result();
	}
}