<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<div class="page-content">	
	<!-- BEGIN PAGE HEADER-->
	<div class="page-bar">
		<ul class="page-breadcrumb">
			<li>
				<i class="fa fa-home"></i>
				<a href="index.html">Home</a>
				<i class="fa fa-angle-right"></i>
			</li>
			<li>
				<a href="#">Dashboard</a>
			</li>
		</ul>
	</div>
	<h3 class="page-title">
	Dashboard <small>reports & statistics</small>
	</h3>
	<!-- END PAGE HEADER-->
	<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12 ">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet box green ">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i>
							</div>
						</div>
						<div class="portlet-body form">
							<form class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>admin/<?php echo $this->uri->segment(2); ?>/delete/<?php echo $pengurus->id_pengurus; ?>">
								<div class="form-body">
									<div class="form-group">
										<label class="col-md-3 control-label">Nama</label>
										<div class="col-md-4">
											<input type="text" name="nama_pengurus" class="form-control" required placeholder="Masukkan Nama">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Tahun Pengurus</label>
										<div class="col-md-4">
											<input type="text" name="tahun_kepengurusan" class="form-control" required placeholder="Masukkan Tahun">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Kelas Pengurus</label>
										<div class="col-md-4">
											<input type="text" name="kelas_pengurus" class="form-control" required placeholder="Masukkan Kelas">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Jabatan Pengurus</label>
										<div class="col-md-4">
											<input type="text" name="jabatan_pengurus" class="form-control" required placeholder="Masukkan Jabatan">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Foto</label>
										<div class="col-md-4">
											<input type="file" name="foto" required>
										</div>
									</div>
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="submit" class="btn green">Submit</button>
											<button type="button" class="btn default">Cancel</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
				</div>
				
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
</div>
</div>
<!-- END CONTENT -->
