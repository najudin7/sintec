<?php
	class Auth extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model('admin_model');
		}

		public function index() {
			$this->load->view ('sintec/login');
		}

		public function proses_login() { 

        	$username = $this->input->post('username'); 
        	$password = $this->input->post('password'); 

        	$login = $this->admin_model->cek_user($username, $password); 

        if (!empty($login)) { 
            // login berhasil 
            $this->session->set_userdata($login); 
            redirect(base_url('admin')); 
        } else { 
            // login gagal 
            $this->session->set_flashdata('gagal', 'Username atau Password Salah!'); 
            redirect(base_url('auth')); 
        } 
    } 

	}