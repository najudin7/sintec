<?php
	class Main extends CI_Controller{
		var $data;
		function __construct(){
			parent::__construct();
			$this->load->library('template_public');
			$this->load->model('public_model');
			$this->load->model('artikel/artikel_model');
			$this->load->model('tutorial/tutorial_model');
			$this->load->model('portofolio/portofolio_model');
		}
		function index(){
			$data = $this->data;
			$data['pengurus'] = $this->public_model->getPengurus();
			$this->template_public->display('sintec/home', $data);
		}
		function artikel(){
			$data = $this->data;
			$data['artikel'] = $this->public_model->getArtikel();
			$this->template_public->display('sintec/artikel', $data);
		}
		function tutorial(){
			$data = $this->data;
			$data['tutorial'] = $this->public_model->getTutorial();
			$this->template_public->display('sintec/tutorial', $data);
		}
		function portofolio(){
			$data = $this->data;
			$data['portofolio'] = $this->public_model->getPortofolio();
			$this->template_public->display('sintec/portofolio', $data);
		}
		
		//public function detailartikel($id=null){
		//	
		//	$data['artikel'] = $this->artikel_model->getArtikelbyid($id);
		//	$this->load->view('sintec/detail_artikel', $data);
		//	$this->template_public->display('sintec/template');
		
		//$data['sidebar'] = $this->sidebar();
		//$data['artikel'] = $this->db->get_where('content',array('id_artikel'=>$id))->row_array();
		//$this->template->load('sintec/template','sintec/detail_artikel',$data);
		//}

	}