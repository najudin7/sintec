--
-- Database: `sintec`
--

-- --------------------------------------------------------

--
-- Table structure for table `artikel`
--

CREATE TABLE `artikel` (
  `id_artikel` int(11) NOT NULL,
  `judul_artikel` varchar(250) NOT NULL,
  `seo_artikel` varchar(250) NOT NULL,
  `isi_artikel` text NOT NULL,
  `tanggal_posting` date NOT NULL,
  `viewed` int(11) NOT NULL,
  `kategori_artikel` int(11) NOT NULL,
  `gambar_artikel` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artikel`
--

INSERT INTO `artikel` (`id_artikel`, `judul_artikel`, `seo_artikel`, `isi_artikel`, `tanggal_posting`, `viewed`, `kategori_artikel`, `gambar_artikel`) VALUES
(305, 'Mengapa Kita harus belajar Coding ???', 'mengapa-kita-harus-belajar-coding-', '<p style="text-align: justify;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Di era modern ini seorang programmer benar &ndash; benar sangat dibutuhkan. Karna perkembangan teknologi yang sangat begitu pesat. Kita bisa mengetahui skor sepakbola liga spanyol, inggris, dan italia tanpa harus kesana. Belanja pun sekarang bisa tanpa kita pindah dari tempat kita atau tepatnya belanja online. Anak &ndash; anak kecil pun sudah canggih memainkan tablet/smartphone-Nya. Mencari informasi pun sekarang mudah hanya tingga kita pakai mesin pencari seperti google,bing dan yahoo. Sekarang semua telah berubah mulai dari perindrustian,kebiasaan,dan pola hidup.</p>\r\n\r\n<p style="text-align: justify;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Lantas siapa yang merubah itu semua ? Siapa yang menciptakan jejaring sosial seperti facebok,twitter,path,instagram ? siapa yang menciptakan mesin pencari seperti google,yahoo,dan bing ? siapa yang menciptakan sistem operasi seperti windows,iOS,android ? mereka adalah seorang programmer yang menulis baris demi baris code.</p>\r\n\r\n<p style="text-align: justify;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Kita tahu perindrustian teknologi berkembang begitu cepat. Seperti 2 tahun lalu blackberry begitu populernya hingga semua orang ingin memilikinya tetapi sekarang Blackberry dijatuhkan oleh android dan iOS milik apple. Perubahan begitu cepat terjadi, cracking industri dimana-mana dan inovasi sulit terbendung. Memang begitulah dunia, kejam. Siapa yang lebih kuat dia akan bisa bertahan. Yang tidak kuat terhadap perubahan akan tergilas. Yang jadi pertanyaan adalah, apa peran kita dalam perubahan itu? Seolah kita hanya menonton dan mengikuti arus perubahan, hanya menjadi yang mencoba bertahan dengan perubahan, dan berusaha mengikuti perubahan. Lantas siapa yang membuat perubahan itu? Ya, mereka adalah negara-negara yang memiliki inovasi, negara-negara yang memiliki programmer-programmer hebat yang selalu menciptakan hal-hal baru. Kita masih jadi penonton pertarungan mereka. Untuk era ini masih di pegang Amerika dengan Silicon Valley &ndash; Nya. Tetapi Indonesia sekarang mencoba bangkit dan sudah banyak startup teknologi yang dibangun di Indonesia seperti Tokopedia,BukaLapak,Traveloka,Doku,Dll.</p>\r\n\r\n<p style="text-align: justify;">Itulah mengapa harus ada dari kita yang benar-benar belajar coding. Semakin banyak yang mempelajarinya, maka akan semakin baik. Semakin besar pula potensi akan munculnya karya-karya kita, munculnya inovasi-inovasi di tanah air. Syukur-syukur ke depannya kita bisa menjadi negara yang bisa melahirkan produk baru yang mendunia, dan kemudian ikut serta dalam pertarungan, dan tidak hanya menjadi penonton.</p>\r\n\r\n<p style="text-align: justify;">Mungkin terlalu tinggi jika kita membicarakan dunia dan negara. Mari kita persempit, kita bicara tentang mengapa kita sebagai seorang individu perlu belajar coding. Steve Jobs pernah berkata, &ldquo;Everybody in this country should learn how to program a computer, because it teaches you how to think.&rdquo;, bahwa setiap orang harus belajar cara memprogram komputer / coding, karena itu mengajarkan bagaimana cara berpikir.</p>\r\n\r\n<p style="text-align: justify;">Benar sekali, dengan belajar coding atau memprogram komputer, kita akan belajar bagaimana cara berpikir yang baik, tertruktur, sistematis dan efisien. Itu akan sangat berguna sekali dalam kehidupan kita, terutama dalam menyelesaikan masalah kita.</p>\r\n\r\n<p style="text-align: justify;">Dan yang paling penting adalah, ketika kita memiki skill coding, kita akan leluasa untuk berkarya. Kita punya ide briliant untuk membuat sebuah aplikasi atau web yang akan menyelesaikan banyak masalah, jika kita memiliki skill coding, kita hanya perlu membuatnya. Kita akan leluasa untuk mengembangkan ide. Tidak dibatasi ketidakmempuan kita membuatnya. Ketika kita punya ide membuat game yang bagus, kita hanya perlu membuatnya. Contohnya Dong Nguyen, ketika dia punya ide untuk membuat game Flappy Bird, dia hanya tinggal membuatnya. Bahkan disela-sela kesibukan dia bekerja. Dan kita tahu, betapa boomingnya Flappy Bird itu. Dan sekarang dia membuat sebuah game baru bernama Swing Copters. Menyenangkan bukan.</p>\r\n\r\n<p style="text-align: justify;">Untuk belajar coding, seseorang tidak mesti masuk kuliah jurusan komputer. Siapa pun bisa. Itu tidak seperti dokter, yang kita harus masuk jurusan kedokteran dulu untuk menjadi seorang dokter atau melakukan praktek dokter. Tidak juga seperti seorang tentara, yang harus masuk akademi dulu. Siapa saja bisa belajar coding bahkan menjadi programmer. Siapa saja bisa berkarya dengan coding, apapun profesinya. Seorang guru bisa saja belajar coding, untuk membuat aplikasi yang berguna dalam proses mengajarnya. Seorang ibu rumah tangga bisa saja belajar coding untuk membuatkan anaknya sebuah game yang menarik dan mendidik. Ya, siapa saja bisa.</p>\r\n\r\n<p style="text-align: justify;">So, tunggu apa lagi? <strong>Belajar coding</strong> dari sekarang dan berkaryalah!</p>\r\n\r\n<p style="text-align: justify;"><em><strong>Diadaptasi dari : codepolitan.com</strong></em></p>\r\n', '2016-06-08', 0, 401, 'coding.jpg'),
(310, 'Jepang Berencana Memasukkan Pemrograman Sebagai Mata Pelajaran di Sekolah', 'jepang-berencana-memasukkan-pemrograman-sebagai-mata-pelajaran-di-sekolah', '<p>Pemerintah Jepang melalui MEXT (<em>Ministry of Education, Culture, Sports, Science, and Technology</em>) sedang berencana untuk memasukkan pemrograman komputer sebagai mata pelajaran wajib di sekolah dasar, sekolah menegah pertama, dan sekolah menengah atas. Untuk sekolah dasar, pemerintah Jepang berencana untuk memulainya pada tahun 2020, diikuti oleh sekolah menegah pertama pada tahun 2021, dan sekolah menengah atas pada tahun 2022.</p>\r\n\r\n<p>Di beberapa kota seperti Koga di Prefektur Ibaraki dan Takeo di Prefektur Saga, pemrograman komputer telah masuk ke sekolah dasar melalui kerjasama dengan beberapa perusahaan dan relawan. Hal yang dilakukan oleh pemerintah kota setempat dilaporkan berhasil meningkatkan minat belajar siswa. Keberhasilan kedua kota ini menjadi acuan bagi kementrian pendidikan Jepang dalam menerapkan kelas pemrograman komputer di daerah lain.</p>\r\n\r\n<p>Langkah yang dilakukan oleh pemerintah Jepang ini mengikuti keberhasilan negara-negara lain seperti Israel dan Inggris yang lebih dahulu menyisipkan pemrograman sebagai mata pelajaran wajib di sekolah. Inggris bahkan telah mewajibkan siswa yang berumur lebih dari lima tahun untuk belajar.</p>\r\n\r\n<p>Akankah pemrograman menjadi salah satu mata pelajaran wajib di sekolah-sekolah Indonesia di masa yang akan datang? Kita tunggu saja :).</p>\r\n\r\n<p><strong>sumber : codepolitan.com</strong></p>\r\n', '2016-06-11', 0, 401, '');

-- --------------------------------------------------------

--
-- Table structure for table `icon`
--

CREATE TABLE `icon` (
  `id_icon` int(11) NOT NULL,
  `nama_icon` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `icon`
--

INSERT INTO `icon` (`id_icon`, `nama_icon`) VALUES
(201, 'icon-user'),
(202, 'icon-user-female'),
(203, 'icon-users'),
(204, 'icon-user-follow'),
(205, 'icon-user-following'),
(206, 'icon-trophy'),
(207, 'icon-speedometer'),
(208, 'icon-social-youtube'),
(209, 'icon-social-twitter'),
(210, 'icon-social-tumblr'),
(211, 'icon-social-facebook'),
(212, 'icon-screen-tablet'),
(213, 'icon-screen-smartphone'),
(214, 'icon-screen-desktop');

-- --------------------------------------------------------

--
-- Table structure for table `kategoriartikel`
--

CREATE TABLE `kategoriartikel` (
  `id_kategoriartikel` int(11) NOT NULL,
  `nama_kategoriartikel` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategoriartikel`
--

INSERT INTO `kategoriartikel` (`id_kategoriartikel`, `nama_kategoriartikel`) VALUES
(401, 'Game');

-- --------------------------------------------------------

--
-- Table structure for table `kepengurusan`
--

CREATE TABLE `kepengurusan` (
  `id_kepengurusan` int(11) NOT NULL,
  `tahun_kepengurusan` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kepengurusan`
--

INSERT INTO `kepengurusan` (`id_kepengurusan`, `tahun_kepengurusan`) VALUES
(1, '2015 / 2016');

-- --------------------------------------------------------

--
-- Table structure for table `menuadmin`
--

CREATE TABLE `menuadmin` (
  `id_menuadmin` int(11) NOT NULL,
  `nama_menuadmin` varchar(100) NOT NULL,
  `link_menuadmin` varchar(100) NOT NULL,
  `is_aktif` tinyint(1) NOT NULL,
  `urutan_ke` int(11) NOT NULL,
  `icon_menuadmin` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menuadmin`
--

INSERT INTO `menuadmin` (`id_menuadmin`, `nama_menuadmin`, `link_menuadmin`, `is_aktif`, `urutan_ke`, `icon_menuadmin`) VALUES
(101, 'Home', '', 1, 1, 'icon-screen-desktop'),
(102, 'Artikel', 'artikel', 1, 5, 'icon-user'),
(103, 'Menu Admin', 'menuadmin', 1, 7, 'icon-screen-tablet'),
(104, 'Pengurus', 'pengurus', 1, 3, 'icon-users'),
(105, 'Tutorial', 'tutorial', 1, 4, 'icon-user-following'),
(106, 'Portofolio', 'portofolio', 1, 6, 'icon-user');

-- --------------------------------------------------------

--
-- Table structure for table `pengurus`
--

CREATE TABLE `pengurus` (
  `id_pengurus` int(11) NOT NULL,
  `nama_pengurus` varchar(255) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(32) NOT NULL,
  `tahun_kepengurusan` int(11) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `kelas_pengurus` varchar(20) NOT NULL,
  `jabatan_pengurus` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengurus`
--

INSERT INTO `pengurus` (`id_pengurus`, `nama_pengurus`, `username`, `password`, `tahun_kepengurusan`, `foto`, `kelas_pengurus`, `jabatan_pengurus`) VALUES
(1, 'Petra Bayu Pangestu', 'petra', '3e39e6920fceb1669584dec1eff55175', 2015, 'Petra.jpg', 'XI MIIA 6', 'Ketua'),
(2, 'Muhammad Alfan Khoiruddin', 'alfan', '1b0b0c715b03257541fe208cf251ef42', 2015, 'alfan.jpg', 'XI MIIA 2', 'Wakil Ketua'),
(3, 'Ahmad Naajuddin', 'naju', 'b83ff3fdcd4013df35cb2a6b1e4a0cd9', 2015, 'naju.png', 'XI MIIA 2', 'Sie Desain Graphic'),
(5, 'Togi Verianto', 'togi', '8b7de08decf9b190e6da4535bc883f45', 2015, 'Togi.jpg', 'XI MIIA 3', 'Sie Programming'),
(6, 'Febriora Navia', 'febri', '296aadbf4ebe1379615e81800dab7777', 2015, 'febriora.png', 'XI MIIA 5', 'Sekretaris'),
(7, 'Fifi Maghfirotun Nisa''', 'fifi', 'cfc723a39d137f8428d8eeec9cebd384', 2015, 'Fifi.jpg', 'XI MIIA 2', 'Sekretaris'),
(8, 'Aulia Fiki Aprilia', 'aulia', '9d43863a1e1b460a4632f7c31420d6c3', 2015, 'Aulia.jpg', 'XI MIIA 5', 'Bendahara'),
(9, 'Aldiansyah', 'aldi', 'daf036f7f77e11a342e9520ff8fc256d', 2015, 'Aldi.jpg', 'XI IIS', 'Bendahara'),
(10, 'As''ad Rofianto', 'asad', '3066ae72739e663244a565eebc73612d', 2015, 'Asad.jpg', 'XI IIS', 'Pengurus'),
(11, 'Ach. Theo Syafi''i', 'theo', '478d5000594ef50c56e98681961aee6d', 2015, 'theo.jpg', 'XI MIIA 2', 'Pengurus'),
(12, 'Bayu Agung S.', 'bayu', '92360c2c392c85b23f38c188996f8d74', 2015, 'Bayu.jpg', 'XI IIS', 'Pengurus'),
(13, 'Mei Mukti Wardhana', 'dana', '45b1c901aa5d4747f1d123a73f9b4482', 2015, 'dana.jpg', 'XI MIIA 3', 'Pengurus'),
(14, 'Achmad Wildan', 'wildan', 'ac174e67e662c4d31dcc2e8b16358024', 2015, 'wildan.jpg', 'XI MIIA 4', 'Pengurus'),
(15, 'Guntur Danang Prasetya', 'guntur', 'ef803eebfaaaee381a84a353e05cae91', 2015, 'Guntur.jpg', 'XI MIIA 6', 'Pengurus'),
(16, 'Savira Octavia', 'savira', '2dcb126e703923c274b3c536a5c9fc3d', 2015, 'savira.jpg', 'XI IIS', 'Pengurus'),
(17, 'Ahmad Faizzudin', 'faiz', '67f27ddf40a0dd24fe50fbe33c4460bb', 2015, 'faiz.jpg', 'XI IIS', 'Pengurus'),
(18, 'Jenny Azizah', 'jenny', '067fccb09c1d91f4f0c5d6d21d5355d9', 2015, 'jenny.jpg', 'XI IBB', 'Pengurus');

-- --------------------------------------------------------

--
-- Table structure for table `submenuadmin`
--

CREATE TABLE `submenuadmin` (
  `id_submenuadmin` int(11) NOT NULL,
  `id_menuadmin` int(11) NOT NULL,
  `nama_submenuadmin` varchar(100) NOT NULL,
  `is_aktif` tinyint(1) NOT NULL,
  `urutan_ke` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tutorial`
--

CREATE TABLE `tutorial` (
  `id_tutorial` int(11) NOT NULL,
  `judul_tutorial` varchar(255) NOT NULL,
  `seo_tutorial` varchar(255) NOT NULL,
  `isi_tutorial` text NOT NULL,
  `tanggal_posting` date NOT NULL,
  `viewed` int(11) NOT NULL,
  `kategori_tutorial` int(11) NOT NULL,
  `gambar_tutorial` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `artikel`
--
ALTER TABLE `artikel`
  ADD PRIMARY KEY (`id_artikel`),
  ADD KEY `kategori_artikel` (`kategori_artikel`),
  ADD KEY `kategori_artikel_2` (`kategori_artikel`);

--
-- Indexes for table `icon`
--
ALTER TABLE `icon`
  ADD PRIMARY KEY (`id_icon`);

--
-- Indexes for table `kategoriartikel`
--
ALTER TABLE `kategoriartikel`
  ADD PRIMARY KEY (`id_kategoriartikel`);

--
-- Indexes for table `kepengurusan`
--
ALTER TABLE `kepengurusan`
  ADD PRIMARY KEY (`id_kepengurusan`);

--
-- Indexes for table `menuadmin`
--
ALTER TABLE `menuadmin`
  ADD PRIMARY KEY (`id_menuadmin`);

--
-- Indexes for table `pengurus`
--
ALTER TABLE `pengurus`
  ADD PRIMARY KEY (`id_pengurus`);

--
-- Indexes for table `submenuadmin`
--
ALTER TABLE `submenuadmin`
  ADD PRIMARY KEY (`id_submenuadmin`),
  ADD KEY `id_menuadmin` (`id_menuadmin`);

--
-- Indexes for table `tutorial`
--
ALTER TABLE `tutorial`
  ADD PRIMARY KEY (`id_tutorial`),
  ADD KEY `kategori_tutorial` (`kategori_tutorial`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `artikel`
--
ALTER TABLE `artikel`
  MODIFY `id_artikel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=311;
--
-- AUTO_INCREMENT for table `icon`
--
ALTER TABLE `icon`
  MODIFY `id_icon` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=215;
--
-- AUTO_INCREMENT for table `kategoriartikel`
--
ALTER TABLE `kategoriartikel`
  MODIFY `id_kategoriartikel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=402;
--
-- AUTO_INCREMENT for table `kepengurusan`
--
ALTER TABLE `kepengurusan`
  MODIFY `id_kepengurusan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `menuadmin`
--
ALTER TABLE `menuadmin`
  MODIFY `id_menuadmin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;
--
-- AUTO_INCREMENT for table `pengurus`
--
ALTER TABLE `pengurus`
  MODIFY `id_pengurus` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `submenuadmin`
--
ALTER TABLE `submenuadmin`
  MODIFY `id_submenuadmin` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tutorial`
--
ALTER TABLE `tutorial`
  MODIFY `id_tutorial` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
