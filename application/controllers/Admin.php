<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	var $data;
	function __construct(){
		parent::__construct();
		if($this->session->userdata('username')!='naju'){
			redirect(base_url().'auth');
		}
		$this->load->library('template_admin');
		$this->load->model('admin_model');
		$this->load->model('menuadmin/menuadmin_model');
		$this->load->model('icon/icon_model');
		$this->load->model('artikel/artikel_model');
		$this->load->model('tutorial/tutorial_model');
		$this->load->model('portofolio/portofolio_model');
		$this->load->model('kategoriartikel/kategoriartikel_model');
		$this->load->model('kategoritutorial/kategoritutorial_model');
		$this->load->model('kategoriportofolio/kategoriportofolio_model');
		$this->load->model('public_model');

		$this->data['sidebar'] = $this->admin_model->getSidebar();
		$this->data['breadcrumb'] = $this->admin_model->getBreadcrumb();
	}
	
	public function index(){
		$data = $this->data;
		$this->template_admin->display('admin/home', $data);
	}
	public function artikel($aksi = 'index', $id=null){
		$data = $this->data;
		switch ($aksi) {
			case 'index':
				$data['artikel'] = $this->artikel_model->getArtikel();
				$this->template_admin->display('admin/content/artikel/index', $data);
				break;
			case 'add':
				$data['kategori_artikel'] = $this->kategoriartikel_model->getKategoriartikel();
				$this->template_admin->display('admin/content/artikel/add', $data);
				break;
			case 'edit':
			    $data['artikel'] = $this->artikel_model->getArtikelbyid($id);
				$this->template_admin->display('admin/content/artikel/edit', $data);
				break;
			case 'insert':
				$this->artikel_model->insert();
				break;
			case 'update':
				$this->artikel_model->update($id);
				redirect('admin/artikel');
				break;
			case 'delete':
				$this->artikel_model->delete($id);
				redirect('admin/artikel');
				break;
			default:
				# code...
				break;
		}
	}

	public function tutorial($aksi = 'index', $id=null){
		$data = $this->data;
		switch ($aksi) {
			case 'index':
				$data['tutorial'] = $this->tutorial_model->getTutorial();
				$this->template_admin->display('admin/content/tutorial/index', $data);
				break;
			case 'add':
				$data['kategori_tutorial'] = $this->kategoritutorial_model->getKategoriTutorial();
				$this->template_admin->display('admin/content/tutorial/add', $data);
				break;
			case 'edit':
			    $data['tutorial'] = $this->tutorial_model->getTutorialbyid($id);
				$this->template_admin->display('admin/content/tutorial/edit', $data);
				break;
			case 'insert':
				$this->tutorial_model->insert();
				break;
			case 'update':
				$this->tutorial_model->update($id);
				redirect('admin/tutorial');
				break;
			case 'delete':
				$this->tutorial_model->delete($id);
				redirect('admin/tutorial');
				break;
			default:
				# code...
				break;
		}
	}
	public function portofolio($aksi = 'index', $id=null){
		$data = $this->data;
		switch ($aksi) {
			case 'index':
				$data['portofolio'] = $this->portofolio_model->getPortofolio();
				$this->template_admin->display('admin/content/portofolio/index', $data);
				break;
			case 'add':
				$data['kategori_portofolio'] = $this->kategoriportofolio_model->getKategoriPortofolio();
				$this->template_admin->display('admin/content/portofolio/add', $data);
				break;
			case 'edit':
			    $data['portofolio'] = $this->portofolio_model->getPortofoliobyid($id);
				$this->template_admin->display('admin/content/portofolio/edit', $data);
				break;
			case 'insert':
				$this->portofolio_model->insert();
				break;
			case 'update':
				$this->portofolio_model->update($id);
				redirect('admin/portofolio');
				break;
			case 'delete':
				$this->portofolio_model->delete($id);
				redirect('admin/portofolio');
				break;
			default:
				# code...
				break;
		}
	}
	function pengurus($aksi='index', $id=null){
			$data = $this->data;
			switch ($aksi) {
				case 'index':
				$data['pengurus'] =$this->public_model->getPengurus();
					$this->template_admin->display('admin/content/pengurus/index', $data);
					break;

				case 'add' :
				$data['icon'] = $this->icon_model->getIcon();
				$data['available_index'] = $this->menuadmin_model->getAvailableIndex();
				$this->template_admin->display('admin/content/pengurus/add', $data);
				break;

				case 'insert':
				$this->admin_model->insert();
					break;

				case 'edit':
				$data['icon'] = $this->icon_model->getIcon();
				$data['pengurus'] = $this->admin_model->getPengurus($id);
				$this->template_admin->display('admin/content/pengurus/edit', $data);
				break;

				case 'update':
					$this->admin_model->update($id);
					redirect('admin/pengurus');
					break;

				case 'delete':
					$this->admin_model->delete($id);
					redirect('admin/pengurus');
					break;
				
				default:
					# code...
					break;
			}
	}
	public function menuadmin($aksi = 'index', $id=null){
		$data = $this->data;
		switch ($aksi) {
			case 'index':
				$data['menuadmin'] = $this->menuadmin_model->getMenuadmin();
				$this->template_admin->display('admin/content/menuadmin/index', $data);
				break;
			case 'add':
				$data['icon'] = $this->icon_model->getIcon();
				$data['available_index'] = $this->menuadmin_model->getAvailableIndex();
				$this->template_admin->display('admin/content/menuadmin/add', $data);
				break;
			case 'edit':
				$data['icon'] = $this->icon_model->getIcon();
				$data['available_index'] = $this->menuadmin_model->getAvailableIndex();
				$data['menuadmin'] = $this->menuadmin_model->getMenuadminById($id);
				$this->template_admin->display('admin/content/menuadmin/edit', $data);
				break;
			case 'insert':
				$this->menuadmin_model->insert();
				redirect('admin/menuadmin');
				break;
			case 'update':
				$this->menuadmin_model->update($id);
				redirect('admin/menuadmin');
				break;
			case 'delete':
				$this->menuadmin_model->delete($id);
				redirect('admin/menuadmin');
				break;
			default:
				# code...
				break;
		}
	}
}
