<?php
	Class Admin_model extends CI_Model{

		function insert(){
		$nama = $this->input->post('nama_pengurus');
		$tahun = $this->input->post('tahun_kepengurusan');
		$kelas = $this->input->post('kelas_pengurus');
		$jabatan = $this->input->post('jabatan_pengurus');
		
		$config = array(
					'allowed_types' => 'jpg|jpeg|png',
					'upload_path' => 'images/pengurus'
			);
			$this->load->library('upload', $config);
			$uploading = $this->upload->do_upload('foto');

			$data_pengurus = $this->upload->data();
			$foto = $data_pengurus['file_name'];

			if (!$uploading) {
				$this->session->set_flashdata('message_error_upload', $this->upload->display_errors());
				//redirect('admin/pengurus/add');
				echo $this->upload->display_errors();
			}else{

		$data =  [
		'nama_pengurus'=>$nama,
		'tahun_kepengurusan'=>$tahun,
		'kelas_pengurus'=>$kelas,
		'jabatan_pengurus'=>$jabatan,
		'foto'=>$foto
		];
		$this->db->insert('pengurus', $data);
		redirect('admin/pengurus');
		}
	}

		public function update($id){
		$nama = $this->input->post('nama_pengurus');
		$tahun = $this->input->post('tahun_kepengurusan');
		$kelas = $this->input->post('kelas_pengurus');
		$jabatan = $this->input->post('jabatan_pengurus');
		
		$data =  [
		'nama_pengurus'=>$nama,
		'tahun_kepengurusan'=>$tahun,
		'kelas_pengurus'=>$kelas,
		'jabatan_pengurus'=>$jabatan,
		'foto'=>$foto
		];
		$where = [
				'id_pengurus' => $id
			     ];

			$this->db->update('pengurus', $data, $where);
		 }

		 function delete($id_pengurus) {
		 	$this->db->where('id_pengurus',$id_pengurus);
		 	$this->db->delete('pengurus');
		 }

		 public function cek_user
		 ($username,$password) {
		 	      $this->db->where('username' , $username);
		 	      $this->db->where('password', md5 ($password));
		 		$query = $this->db->get('pengurus');
		 		return $query->row_array();	
		 	}
		 
	
        public function getPengurus($id){
			$pengurus = $this->db->query("SELECT * FROM pengurus WHERE id_pengurus= '$id'");
			return $pengurus->row();
		} 
		public function getSidebar(){
			$sidebar = $this->db->query("SELECT * FROM menuadmin WHERE is_aktif = '1' ORDER BY urutan_ke ASC");
			$sidebar = $sidebar->result();
			return $sidebar;
		}
		public function getBreadcrumb(){
			echo $this->uri->segment(2);
		}
	}

	
?>