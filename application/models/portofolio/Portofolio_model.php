<?php
	class Portofolio_model extends CI_Model{
		function __construct(){
			parent::__construct();
			
		}
		
		public function getPortofolio(){
			$portofolio = $this->db->query("SELECT * FROM portofolio ORDER BY id_portofolio ASC");
			$portofolio = $portofolio->result();
			return $portofolio;
		}

		public function getPortofoliobyid($id){
			$portofolio = $this->db->query("SELECT * FROM portofolio WHERE id_portofolio = '$id'");
			return $portofolio->row();
		}
		function insert(){
			$judul = $this->input->post('judul_portofolio');
			$author = $this->input->post('author');
			$tanggal_posting = date('Y-m-d');
			$kategori = $this->input->post('kategori_portofolio');
			$view = 0;


			$config = array(
					'allowed_types' => 'jpg|jpeg|png',
					'upload_path' => 'images/portofolio'
			);
			$this->load->library('upload', $config);
			$uploading = $this->upload->do_upload('gambar_portofolio');

			$data_portofolio  = $this->upload->data();
			$gambar_portofolio = $data_portofolio['file_name'];

			if (!$uploading) {
				$this->session->set_flashdata('message_error_upload', $this->upload->display_errors());
				//redirect('admin/portofolio/add');
				echo $this->upload->display_errors();
			}else{
				$data = [
					'judul_portofolio' => $judul,
					'author' => $author,
					'tanggal_posting' => $tanggal_posting,
					'kategori_portofolio' => $kategori,
					'viewed' => $view,
					'gambar_portofolio' => $gambar_portofolio
				];
				$this->db->insert('portofolio', $data);
				redirect('admin/portofolio');
			}
			
		}

		 function delete($id_portofolio) {
		 	$this->db->where('id_portofolio',$id_portofolio);
		 	$this->db->delete('portofolio');
		 }


public function update($id){
			$judul = $this->input->post('judul_portofolio');
			$author = $this->input->post('author');
			$tanggal_posting = date('Y-m-d');
			$kategori = $this->input->post('kategori_portofolio');
			$view = 0;
$data =  [
					'judul_portofolio' => $judul,
					'author' => $author,
					'tanggal_posting' => $tanggal_posting,
					'kategori_portofolio' => $kategori,
					'viewed' => $view,
					'gambar_portofolio' => $gambar_portofolio
		];
		$where = [
				'id_portofolio' => $id
			     ];

			$this->db->update('portofolio', $data, $where);
		 }
}
       