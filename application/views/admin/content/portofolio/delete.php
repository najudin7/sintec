<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<div class="page-content">	
	<!-- BEGIN PAGE HEADER-->
	<div class="page-bar">
		<ul class="page-breadcrumb">
			<li>
				<i class="fa fa-home"></i>
				<a href="index.html">Home</a>
				<i class="fa fa-angle-right"></i>
			</li>
			<li>
				<a href="#">Dashboard</a>
			</li>
		</ul>
	</div>
	<h3 class="page-title">
	Dashboard <small>reports & statistics</small>
	</h3>
	<!-- END PAGE HEADER-->
	<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12 ">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet box green ">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i>
							</div>
						</div>
						<div class="portlet-body form">
							<form class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>admin/<?php echo $this->uri->segment(2); ?>/delete" enctype="multipart/form-data">
								<div class="form-body">
									<div class="form-group">
										<label class="col-md-3 control-label">Judul portofolio</label>
										<div class="col-md-4">
											<input type="text" name="judul_portofolio" class="form-control" required placeholder="Masukkan Judul portofolio">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Author</label>
										<div class="col-md-4">
											<input type="text" name="author" class="form-control" required placeholder="Nama Author">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Kategori portofolio</label>
										<div class="col-md-2">
										<select class="form-control" name="kategori_portofolio">
											<?php 
												foreach ($kategori_portofolio as $q_kategori) {
													echo '<option value="'.$q_kategori->id_kategoriportofolio.'">'.$q_kategori->nama_kategoriportofolio.'</option>';
												}
											?>
										</select>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-3 control-label">Gambar portofolio</label>
										<div class="col-md-4">
											<input type="file" name="gambar_portofolio" required>
										</div>
									</div>
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="submit" class="btn green">Submit</button>
											<button type="button" class="btn default">Cancel</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
				</div>
				
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
</div>
</div>
<!-- END CONTENT -->
