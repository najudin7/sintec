<?php
	class Artikel_model extends CI_Model{
		function __construct(){
			parent::__construct();
			$this->load->library('aksa_seo');
		}
		
		public function getArtikel(){
			$artikel = $this->db->query("SELECT * FROM artikel ORDER BY id_artikel ASC");
			$artikel = $artikel->result();
			return $artikel;
		}

		public function getArtikelbyid($id){
			$artikel = $this->db->query("SELECT * FROM artikel WHERE id_artikel = '$id'");
			return $artikel->row();
		}
		function insert(){
			$judul = $this->input->post('judul_artikel');
			$isi = $this->input->post('isi_artikel');
			$tanggal_posting = date('Y-m-d');
			$kategori = $this->input->post('kategori_artikel');
			$seo = $this->aksa_seo->seo_title($judul);
			$view = 0;


			$config = array(
					'allowed_types' => 'jpg|jpeg|png',
					'upload_path' => 'images/artikel'
			);
			$this->load->library('upload', $config);
			$uploading = $this->upload->do_upload('gambar_artikel');

			$data_artikel  = $this->upload->data();
			$gambar_artikel = $data_artikel['file_name'];

			if (!$uploading) {
				$this->session->set_flashdata('message_error_upload', $this->upload->display_errors());
				//redirect('admin/artikel/add');
				echo $this->upload->display_errors();
			}else{
				$data = [
					'judul_artikel' => $judul,
					'isi_artikel' => $isi,
					'tanggal_posting' => $tanggal_posting,
					'kategori_artikel' => $kategori,
					'seo_artikel' => $seo,
					'viewed' => $view,
					'gambar_artikel' => $gambar_artikel
				];
				$this->db->insert('artikel', $data);
				redirect('admin/artikel');
			}
			
		}

		 function delete($id_artikel) {
		 	$this->db->where('id_artikel',$id_artikel);
		 	$this->db->delete('artikel');
		 }


public function update($id){
			$judul = $this->input->post('judul_artikel');
			$isi = $this->input->post('isi_artikel');
			$tanggal_posting = date('Y-m-d');
			$kategori = $this->input->post('kategori_artikel');
			$seo = $this->aksa_seo->seo_title($judul);
			$view = 0;

		$data =  [
		'judul_artikel' => $judul,
					'isi_artikel' => $isi,
					'tanggal_posting' => $tanggal_posting,
					'kategori_artikel' => $kategori,
					'seo_artikel' => $seo,
					'viewed' => $view,
					'gambar_artikel' => $gambar_artikel
		];
		$where = [
				'id_artikel' => $id
			     ];

			$this->db->update('artikel', $data, $where);
		 }
}

       