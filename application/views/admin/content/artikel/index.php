<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<div class="page-content">	
	<!-- BEGIN PAGE HEADER-->
	<div class="page-bar">
		<ul class="page-breadcrumb">
			<li>
				<i class="fa fa-home"></i>
				<a href="index.html">Home</a>
				<i class="fa fa-angle-right"></i>
			</li>
			<li>
				<a href="#">Dashboard</a>
			</li>
		</ul>
	</div>
	<h3 class="page-title">
	Dashboard <small>reports & statistics</small>
	</h3>
	<!-- END PAGE HEADER-->
	<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box grey-cascade">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i>Artikel
							</div>
							
						</div>
						<div class="portlet-body">
							<div class="table-toolbar">
								<div class="row">
									<div class="col-md-6">
										<div class="btn-group">
											<a href="<?php echo base_url(); ?>admin/<?php echo $this->uri->segment(2); ?>/add">
												<button id="sample_editable_1_new" class="btn green">
													Add New <i class="fa fa-plus"></i>
												</button>
											</a>
										</div>
									</div>
									
								</div>
							</div>
							<table class="table table-striped table-bordered table-hover" id="sample_1">
							<thead>
							<tr>
								<th>
									No
								</th>
								<th>
									Judul Artikel
								</th>
								<th>
									Tanggal Posting
								</th>
								<th>
									Kategori
								</th>
								<th>
									Dilihat
								</th>
								<th>
									Aksi
								</th>
								
							</tr>
							</thead>
							<tbody>
							<?php 
								$no = 1;
								foreach ($artikel as $q_artikel) {
									echo '
										<tr class="odd gradeX">
										<td>
											'.$no.'
										</td>
										<td>
											 '.$q_artikel->judul_artikel.'
										</td>
										<td>
											'.$q_artikel->tanggal_posting.'
										</td>
										<td>
											 '.$q_artikel->kategori_artikel.'
										</td>
										<td class="center">
											'.$q_artikel->viewed.'
										</td>
										<td>
											<a href="'.base_url().'admin/artikel/edit/'.$q_artikel->id_artikel.'"><button class="btn btn-primary"><i class="fa fa-pencil"></i></button></a>
											<a href="'.base_url().'admin/artikel/delete/'.$q_artikel->id_artikel.'"><button class="btn btn-danger"><i class="fa fa-eraser"></i></button></a>
										</td>
									</tr>
									';
								$no++;
								}
							?>
							</tbody>
							</table>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
</div>
</div>
<!-- END CONTENT -->
