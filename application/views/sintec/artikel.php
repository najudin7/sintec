<!-- page-banner-section 
					================================================== -->
				<section class="page-banner-section">
					<div class="container">
						<h2>Blog</h2>
						<ul class="page-depth">
							<li><a href="index.html">Koncept</a></li>
							<li><a href="blog.html">Blog Fullwidth</a></li>
						</ul>
					</div>
				</section>
				<!-- End page-banner section -->

				<!-- blog section 
					================================================== -->
				<section class="blog-section">
					<div class="container">
						<div class="row">
							<div class="col-md-9">
								<div class="blog-box">
									<?php 
										foreach ($artikel as $q_artikel) {
											$text = $q_artikel->isi_artikel;
											echo '
												<div class="blog-post">
													<img src="'.base_url().'images/artikel/'.$q_artikel->gambar_artikel.'" alt="">
													<div class="post-content-text">
														<h2><a href="single-post.html">'.$q_artikel->judul_artikel.'</a></h2>
														<ul class="post-tags">
															<li>31 March ,</li>
															<li><a href="#">Kenan Mazhiqi</a> ,</li>
															<li><a href="#">Building</a></li>
														</ul>
														<p>'.substr($text,0,250).'....</p>
							                          <a href="'.base_url().'baca/selanjutnya/'.$q_artikel->id_artikel.'">Read More</a>
													</div>
												</div>
											';
										}
									?>
									

									<ul class="pagination-list">
										<li><a href="#" class="prev-pag">prev</a></li>
										<li><a href="#" class="active">1</a></li>
										<li><a href="#">2</a></li>
										<li><a href="#">3</a></li>
										<li><a href="#" class="next-pag">next</a></li>
									</ul>

								</div>
								
							</div>
							<div class="col-md-3">
								<div class="sidebar">

									<div class="category-widget widget">
										<h2>Categories</h2>
										<ul class="category-list">
											<li><a href="#">building <span>18</span></a></li>
											<li><a href="#">construction <span>06</span></a></li>
											<li><a href="#">painting <span>12</span></a></li>
											<li><a href="#">isolation <span>15</span></a></li>
											<li><a href="#">electricy <span>6</span></a></li>
											<li><a href="#">architecture <span>4</span></a></li>
										</ul>
									</div>

									<div class="archieve-widget widget">
										<h2>archieve</h2>
										<ul class="archieve-list">
											<li><a href="#">September 2014</a></li>
											<li><a href="#">Octomber 2014</a></li>
											<li><a href="#">November 2014</a></li>
											<li><a href="#">December 2014</a></li>
											<li><a href="#">Jannuary 2015</a></li>
										</ul>
									</div>

									<div class="text-widget widget">
										<h2>Text Widget</h2>
										<div class="text-box">
											<p>Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
											<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci.</p>
										</div>
									</div>

									<div class="flickr-widget widget">
										<h2>Flickr widget</h2>
										<ul class="flickr">
											<li><a href="#"><img src="<?php echo base_url(); ?>assets/sintec/upload/flickr/1.jpg" alt=""></a></li>
											<li><a href="#"><img src="<?php echo base_url(); ?>assets/sintec/upload/flickr/2.jpg" alt=""></a></li>
											<li><a href="#"><img src="<?php echo base_url(); ?>assets/sintec/upload/flickr/3.jpg" alt=""></a></li>
											<li><a href="#"><img src="<?php echo base_url(); ?>assets/sintec/upload/flickr/4.jpg" alt=""></a></li>
											<li><a href="#"><img src="<?php echo base_url(); ?>assets/sintec/upload/flickr/5.jpg" alt=""></a></li>
											<li><a href="#"><img src="<?php echo base_url(); ?>assets/sintec/upload/flickr/6.jpg" alt=""></a></li>
										</ul>
									</div>

									<div class="tags-widget widget">
										<h2>TAG Widget</h2>
										<ul class="tags-list">
											<li><a href="#">Building</a></li>
											<li><a href="#">interior</a></li>
											<li><a href="#">kitchen</a></li>
											<li><a href="#">isolation</a></li>
											<li><a href="#">energy</a></li>
										</ul>
									</div>

								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- End blog section -->