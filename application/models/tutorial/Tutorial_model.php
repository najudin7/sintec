<?php
	class Tutorial_model extends CI_Model{
		function __construct(){
			parent::__construct();
			$this->load->library('aksa_seo');
		}
		
		public function getTutorial(){
			$tutorial = $this->db->query("SELECT * FROM tutorial ORDER BY id_tutorial ASC");
			$tutorial = $tutorial->result();
			return $tutorial;
		}

		public function getTutorialbyid($id){
			$tutorial = $this->db->query("SELECT * FROM tutorial WHERE id_tutorial = '$id'");
			return $tutorial->row();
		}
		function insert(){
			$judul = $this->input->post('judul_tutorial');
			$isi = $this->input->post('isi_tutorial');
			$tanggal_posting = date('Y-m-d');
			$kategori = $this->input->post('kategori_tutorial');
			$seo = $this->aksa_seo->seo_title($judul);
			$view = 0;


			$config = array(
					'allowed_types' => 'jpg|jpeg|png',
					'upload_path' => 'images/tutorial'
			);
			$this->load->library('upload', $config);
			$uploading = $this->upload->do_upload('gambar_tutorial');

			$data_tutorial  = $this->upload->data();
			$gambar_tutorial = $data_tutorial['file_name'];

			if (!$uploading) {
				$this->session->set_flashdata('message_error_upload', $this->upload->display_errors());
				//redirect('admin/tutorial/add');
				echo $this->upload->display_errors();
			}else{
				$data = [
					'judul_tutorial' => $judul,
					'isi_tutorial' => $isi,
					'tanggal_posting' => $tanggal_posting,
					'kategori_tutorial' => $kategori,
					'seo_tutorial' => $seo,
					'viewed' => $view,
					'gambar_tutorial' => $gambar_tutorial
				];
				$this->db->insert('tutorial', $data);
				redirect('admin/tutorial');
			}
			
		}

		 function delete($id_tutorial) {
		 	$this->db->where('id_tutorial',$id_tutorial);
		 	$this->db->delete('tutorial');
		 }


public function update($id){
			$judul = $this->input->post('judul_tutorial');
			$isi = $this->input->post('isi_tutorial');
			$tanggal_posting = date('Y-m-d');
			$kategori = $this->input->post('kategori_tutorial');
			$seo = $this->aksa_seo->seo_title($judul);
			$view = 0;

		$data =  [
		'judul_tutorial' => $judul,
					'isi_tutorial' => $isi,
					'tanggal_posting' => $tanggal_posting,
					'kategori_tutorial' => $kategori,
					'seo_tutorial' => $seo,
					'viewed' => $view,
					'gambar_tutorial' => $gambar_tutorial
		];
		$where = [
				'id_tutorial' => $id
			     ];

			$this->db->update('tutorial', $data, $where);
		 }
}

       