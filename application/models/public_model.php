<?php
	class Public_model extends CI_Model{
		function getPengurus(){
			$pengurus = $this->db->get('pengurus');
			return $pengurus->result();
		}
		function getArtikel(){
			$artikel = $this->db->get('artikel');
			return $artikel->result();
		}
		function getTutorial(){
			$tutorial = $this->db->get('tutorial');
			return $tutorial->result();
		}
		function getPortofolio(){
			$portofolio = $this->db->get('portofolio');
			return $portofolio->result();
		}
	}