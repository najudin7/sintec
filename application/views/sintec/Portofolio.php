<section class="page-banner-section">
			<div class="container">
				<h2>Portofolio</h2>
				<ul class="page-depth">
					<li><a href="index.html">Sintec</a></li>
					<li><a href="projects-3col.html">Potofolio 3 Kolom</a></li>
				</ul>
			</div>
</section>
		<!-- End page-banner section -->

		<!-- projects-page section 
			================================================== -->
		<section class="projects-section projects-page-section">
			<div class="container">
				<div class="title-section alt-title">
					<div class="row">
						<div class="col-md-5">
							<h1>Karya Kita</h1>
						</div>
						<div class="col-md-7">
							<ul class="filter">
								<li><a class="active" href="#" data-filter="*">Show All</a></li>
								<li><a href="#" data-filter=".buildings">Buildings</a></li>
								<li><a href="#" data-filter=".interior">Interior</a></li>
								<li><a href="#" data-filter=".energy">Energy</a></li>
								<li><a href="#" data-filter=".isolation">Isolation</a></li>
							</ul>
						</div>
					</div>
				</div>

				<div class="project-box iso-call col3">
				<?php
				foreach ($portofolio as $q_portofolio) {
				echo '
					<div class="project-post buildings isolation">
						<img src="'.base_url().'images/portofolio/'.$q_portofolio->gambar_portofolio.'" alt="">
						<div class="hover-box">
							<div class="inner-hover">
								<h2><a href="single-project.html">'.$q_portofolio->judul_portofolio.'</a></h2>
								<span>'.$q_portofolio->author.'</span>
								<a href="single-project.html" class="link"><i class="fa fa-link"></i></a>
								<a href="'.base_url().'images/portofolio/'.$q_portofolio->gambar_portofolio.'" class="zoom"><i class="fa fa-arrows-alt"></i></a>
							</div>
						</div>
					</div>
					';
				}
				?>
					
				</div>
			</div>
		</section>
		<!-- End projects-page section -->


	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.migrate.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
	<script type="text/javascript" src="js/jquery.imagesloaded.min.js"></script>
  	<script type="text/javascript" src="js/jquery.isotope.min.js"></script>
	<script type="text/javascript" src="js/retina-1.1.0.min.js"></script>
	<script type="text/javascript" src="js/plugins-scroll.js"></script>
	<script type="text/javascript" src="js/script.js"></script>