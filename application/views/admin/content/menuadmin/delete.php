<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<div class="page-content">
	<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
	<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Modal title</h4>
				</div>
				<div class="modal-body">
					 Widget settings form goes here
				</div>
				<div class="modal-footer">
					<button type="button" class="btn blue">Save changes</button>
					<button type="button" class="btn default" data-dismiss="modal">Close</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
	<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
	
	<!-- BEGIN PAGE HEADER-->
	<div class="page-bar">
		<ul class="page-breadcrumb">
			<li>
				<i class="fa fa-home"></i>
				<a href="index.html">Home</a>
				<i class="fa fa-angle-right"></i>
			</li>
			<li>
				<a href="#">Dashboard</a>
			</li>
		</ul>
	</div>
	<h3 class="page-title">
	Dashboard <small>reports & statistics</small>
	</h3>
	<!-- END PAGE HEADER-->
	<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12 ">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet box green ">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i>
							</div>
						</div>
						<div class="portlet-body form">
							<form class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>admin/<?php echo $this->uri->segment(2); ?>/delete">
								<div class="form-body">
									<div class="form-group">
										<label class="col-md-3 control-label">Nama Menu Admin</label>
										<div class="col-md-4">
											<input type="text" name="nama" class="form-control" required placeholder="Masukkan Nama Menu Admin">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Link Menu Admin</label>
										<div class="col-md-4">
											<input type="text" name="link" class="form-control" required placeholder="Masukkan Link Menu Admin">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Aktif</label>
										<div class="col-md-9">
											<div class="radio-list">
												<label class="radio-inline">
												<input type="radio" name="aktif" value="1" checked> Ya </label>
												<label class="radio-inline">
												<input type="radio" name="aktif" value="0"> Tidak </label>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Icon</label>
										<div class="col-md-9">
											<div class="radio-list">
												<?php 
													$cek_first = 1;
													foreach ($icon as $q_icon) {
														if($cek_first == 1){
															$cek = 'checked';
															$cek_first = 0;
														}else{
															$cek = '';
														}
														echo '
															<label class="radio-inline">
															<input type="radio" name="icon" value="'.$q_icon->nama_icon.'" '.$cek.'><span aria-hidden="true" class="'.$q_icon->nama_icon.'"></span></label>
														';
													}
												?>
																								
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Urutan Ke</label>
										<div class="col-md-1">
											<select class="form-control" name="urutan">
												<?php 
													foreach ($available_index as $q_available) {
														echo '<option value="'.$q_available.'">'.$q_available.'</option>';
													}
												?>
											</select>
										</div>
									</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="submit" class="btn green">Submit</button>
											<button type="button" class="btn default">Cancel</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
				</div>
				
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
</div>
</div>
<!-- END CONTENT -->
