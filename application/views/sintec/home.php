				<!-- home-section 
					================================================== -->
				<section id="home-section" class="slider1">
					
					<!--
					#################################
						- THEMEPUNCH BANNER -
					#################################
					-->
					<div class="tp-banner-container">
						<div class="tp-banner" >
							<ul>	<!-- SLIDE  -->
								<li data-transition="fade" data-slotamount="7" data-masterspeed="500" data-saveperformance="on"  data-title="Intro Slide">
									<!-- MAIN IMAGE -->
									<img src="<?php echo base_url(); ?>assets/sintec/upload/slide/header.jpg"  alt="slidebg1" data-lazyload="<?php echo base_url(); ?>assets/sintec/upload/slide/header.jpg" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
									<!-- LAYERS -->

									<!-- LAYER NR. 1 -->
									<div class="tp-caption finewide_medium_white lft tp-resizeme rs-parallaxlevel-0"
										data-x="40"
										data-y="160" 
										data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
										data-speed="1000"
										data-start="1200"
										data-easing="Power3.easeInOut"
										data-splitin="none"
										data-splitout="none"
										data-elementdelay="0.1"
										data-endelementdelay="0.1"
										style="z-index: 8; max-width: auto; max-height: auto; white-space: nowrap;">SINTEC <span class="color-skin">CASTLE</span>
									</div>

									<!-- LAYER NR. 1 -->
									<div class="tp-caption medium_thin_grey customin"
										data-x="40"
										data-y="230" 
										data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
										data-speed="1000"
										data-start="1600"
										data-easing="Power3.easeInOut"
										data-splitin="none"
										data-splitout="none"
										data-elementdelay="0.1"
										data-endelementdelay="0.1"
										style="z-index: 8; max-width: auto; max-height: auto; white-space: nowrap;">Ekstracurricular in SMAN 1 PURI Mojokerto
									</div>

									<!-- LAYER NR. 2 -->
									<div class="tp-caption small_text customin tp-resizeme rs-parallaxlevel-0"
										data-x="40"
										data-y="290" 
										data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
										data-speed="1000"
										data-start="2200"
										data-easing="Power3.easeInOut"
										data-splitin="none"
										data-splitout="none"
										data-elementdelay="0.05"
										data-endelementdelay="0.1"
										style="z-index: 7; max-width: auto; max-height: auto; white-space: nowrap;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor<br>  incididunt ut labore et dolore magna aliqua. Ut enim
									</div>

									<!-- LAYER NR. 3 -->
									<div class="tp-caption lfb tp-resizeme rs-parallaxlevel-0"
										data-x="40"
										data-y="370" 
										data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
										data-speed="1000"
										data-start="2500"
										data-easing="Power3.easeInOut"
										data-splitin="none"
										data-splitout="none"
										data-elementdelay="0.1"
										data-endelementdelay="0.1"
										data-linktoslide="next"
										style="z-index: 10; max-width: auto; max-height: auto; white-space: nowrap;"><a href='#' class='trans-btn'>load more</a>
									</div>

									<!-- LAYER NR. 4 -->
									<div class="tp-caption lfr tp-resizeme rs-parallaxlevel-0"
										data-x="220"
										data-y="370" 
										data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
										data-speed="1000"
										data-start="2600"
										data-easing="Power3.easeInOut"
										data-splitin="none"
										data-splitout="none"
										data-elementdelay="0.1"
										data-endelementdelay="0.1"
										data-linktoslide="next"
										style="z-index: 11; max-width: auto; max-height: auto; white-space: nowrap;"><a href='main/portofolio' class='trans-btn2'>Portofolio</a>
									</div>

								</li>
								<!-- SLIDE  -->
								<li data-transition="fade" data-slotamount="7" data-masterspeed="500" data-saveperformance="on"  data-title="Intro Slide">
									<!-- MAIN IMAGE -->
									<img src="<?php echo base_url(); ?>assets/sintec/upload/slide/header2.jpg"  alt="slidebg1" data-lazyload="<?php echo base_url(); ?>assets/sintec/upload/slide/header2.jpg" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
									<!-- LAYERS -->

									<!-- LAYER NR. 1 -->
									<div class="tp-caption finewide_medium_white lft tp-resizeme rs-parallaxlevel-0 center-align"
										data-x="250"
										data-y="160" 
										data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
										data-speed="1000"
										data-start="1200"
										data-easing="Power3.easeInOut"
										data-splitin="none"
										data-splitout="none"
										data-elementdelay="0.1"
										data-endelementdelay="0.1"
										style="z-index: 8; max-width: auto; max-height: auto; white-space: nowrap;">WE Born <span class="thin-skin color-skin">for</span> Technology
									</div>

									<!-- LAYER NR. 1 -->
									<div class="tp-caption medium_thin_grey customin center-align"
										data-x="375"
										data-y="230" 
										data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
										data-speed="1000"
										data-start="1600"
										data-easing="Power3.easeInOut"
										data-splitin="none"
										data-splitout="none"
										data-elementdelay="0.1"
										data-endelementdelay="0.1"
										style="z-index: 8; max-width: auto; max-height: auto; white-space: nowrap;">Tutorial From SINTEC
									</div>

									<!-- LAYER NR. 2 -->
									<div class="tp-caption small_text customin tp-resizeme rs-parallaxlevel-0 center-align"
										data-x="330"
										data-y="290" 
										data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
										data-speed="1000"
										data-start="2200"
										data-easing="Power3.easeInOut"
										data-splitin="none"
										data-splitout="none"
										data-elementdelay="0.05"
										data-endelementdelay="0.1"
										style="z-index: 7; max-width: auto; max-height: auto; white-space: nowrap;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor<br>  incididunt ut labore et dolore magna aliqua. Ut enim
									</div>

									<!-- LAYER NR. 3 -->
									<div class="tp-caption lfb tp-resizeme rs-parallaxlevel-0"
										data-x="410"
										data-y="370" 
										data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
										data-speed="1000"
										data-start="2500"
										data-easing="Power3.easeInOut"
										data-splitin="none"
										data-splitout="none"
										data-elementdelay="0.1"
										data-endelementdelay="0.1"
										data-linktoslide="next"
										style="z-index: 10; max-width: auto; max-height: auto; white-space: nowrap;"><a href='#' class='trans-btn'>load more</a>
									</div>

									<!-- LAYER NR. 4 -->
									<div class="tp-caption lfr tp-resizeme rs-parallaxlevel-0"
										data-x="590"
										data-y="370" 
										data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
										data-speed="1000"
										data-start="2600"
										data-easing="Power3.easeInOut"
										data-splitin="none"
										data-splitout="none"
										data-elementdelay="0.1"
										data-endelementdelay="0.1"
										data-linktoslide="next"
										style="z-index: 11; max-width: auto; max-height: auto; white-space: nowrap;"><a href='main/tutorial' class='trans-btn2'>Tutorial</a>
									</div>

								</li>
								<!-- SLIDE  -->
								<li data-transition="fade" data-slotamount="7" data-masterspeed="500" data-saveperformance="on"  data-title="Intro Slide">
									<!-- MAIN IMAGE -->
									<img src="<?php echo base_url(); ?>assets/sintec/upload/slide/header3.jpg"  alt="slidebg1" data-lazyload="<?php echo base_url(); ?>assets/sintec/upload/slide/header3.jpg" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
									<!-- LAYERS -->

									<!-- LAYER NR. 1 -->
									<div class="tp-caption finewide_medium_white lft tp-resizeme rs-parallaxlevel-0"
										data-x="40"
										data-y="160" 
										data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
										data-speed="1000"
										data-start="1200"
										data-easing="Power3.easeInOut"
										data-splitin="none"
										data-splitout="none"
										data-elementdelay="0.1"
										data-endelementdelay="0.1"
										style="z-index: 8; max-width: auto; max-height: auto; white-space: nowrap;">WE are <span class="color-skin">community</span>
									</div>

									<!-- LAYER NR. 1 -->
									<div class="tp-caption medium_thin_grey customin"
										data-x="40"
										data-y="230" 
										data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
										data-speed="1000"
										data-start="1600"
										data-easing="Power3.easeInOut"
										data-splitin="none"
										data-splitout="none"
										data-elementdelay="0.1"
										data-endelementdelay="0.1"
										style="z-index: 8; max-width: auto; max-height: auto; white-space: nowrap;">BLOG SINTEC
									</div>

									<!-- LAYER NR. 2 -->
									<div class="tp-caption small_text customin tp-resizeme rs-parallaxlevel-0"
										data-x="40"
										data-y="290" 
										data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
										data-speed="1000"
										data-start="2200"
										data-easing="Power3.easeInOut"
										data-splitin="none"
										data-splitout="none"
										data-elementdelay="0.05"
										data-endelementdelay="0.1"
										style="z-index: 7; max-width: auto; max-height: auto; white-space: nowrap;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor<br>  incididunt ut labore et dolore magna aliqua. Ut enim
									</div>

									<!-- LAYER NR. 3 -->
									<div class="tp-caption lfb tp-resizeme rs-parallaxlevel-0"
										data-x="40"
										data-y="370" 
										data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
										data-speed="1000"
										data-start="2500"
										data-easing="Power3.easeInOut"
										data-splitin="none"
										data-splitout="none"
										data-elementdelay="0.1"
										data-endelementdelay="0.1"
										data-linktoslide="next"
										style="z-index: 10; max-width: auto; max-height: auto; white-space: nowrap;"><a href='#' class='trans-btn'>load more</a>
									</div>

									<!-- LAYER NR. 4 -->
									<div class="tp-caption lfr tp-resizeme rs-parallaxlevel-0"
										data-x="220"
										data-y="370" 
										data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
										data-speed="1000"
										data-start="2600"
										data-easing="Power3.easeInOut"
										data-splitin="none"
										data-splitout="none"
										data-elementdelay="0.1"
										data-endelementdelay="0.1"
										data-linktoslide="next"
										style="z-index: 11; max-width: auto; max-height: auto; white-space: nowrap;"><a href='main/blog' class='trans-btn2'>Blog</a>
									</div>
								</li>
							</ul>
							<div class="tp-bannertimer"></div>
						</div>
					</div>
				</section>
				<!-- End home section -->

				<!-- services-offer 
					================================================== -->
				<section class="services-offer-section">
					<div class="container">
						<div class="title-section">
							<h1>Kegiatan</h1>
							<span>Best Our Activity</span>
							<p>Kumpulan Dokumentasi Kegiatan SINTEC
							</p>
						</div>
						<div class="services-box-mas iso-call">
							<div class="services-project default-size">
								<div class="services-gal">
									<img src="<?php echo base_url(); ?>assets/sintec/upload/services/2.jpg" alt="">
									<div class="hover-services">
										<h2><a href="building.html">Diklat Anggota</a></h2>
										<span>Inspecting, Details</span>
									</div>
								</div>
							</div>
							<div class="services-project snd-size">
								<div class="services-gal">
									<img src="<?php echo base_url(); ?>assets/sintec/upload/services/1.jpg" alt="">
									<div class="hover-services">
										<h2><a href="building.html">Seminar UMM</a></h2>
										<span>Painting, autolimar</span>
									</div>
								</div>
							</div>
							<div class="services-project">
								<div class="services-gal">
									<img src="<?php echo base_url(); ?>assets/sintec/upload/services/3.jpg" alt="">
									<div class="hover-services">
										<h2><a href="building.html">Diklat pengurus</a></h2>
										<span>Repair, Oil</span>
									</div>
								</div>
							</div>
							<div class="services-project">
								<div class="services-gal">
									<img src="<?php echo base_url(); ?>assets/sintec/upload/services/4.jpg" alt="">
									<div class="hover-services">
										<h2><a href="building.html">Programming</a></h2>
										<span>Repair</span>
									</div>
								</div>
							</div>
							<div class="services-project">
								<div class="services-gal">
									<img src="<?php echo base_url(); ?>assets/sintec/upload/services/5.jpg" alt="">
									<div class="hover-services">
										<h2><a href="building.html">Desain Graphic</a></h2>
										<span>tire, repair, upgrade</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- End services-offer section -->

				<!-- projects 
					================================================== -->
				<section class="projects-section">
					<div class="container">
						<div class="title-section">
							<h1>Portofolio</h1>
							<span>Best Our Creativity</span>
							<p>Kumpulan karya - karya SINTEC</p>
						</div>
					</div>
					<div class="project-box iso-call">
						<div class="project-post buildings isolation">
							<img src="<?php echo base_url(); ?>assets/sintec/upload/projects/1.jpg" alt="">
							<div class="hover-box">
								<div class="inner-hover">
									<h2><a href="single-project.html">Elegant Building</a></h2>
									<span>bulding, house</span>
									<a href="single-project.html" class="link"><i class="fa fa-link"></i></a>
									<a href="<?php echo base_url(); ?>assets/sintec/upload/projects/1.jpg" class="zoom"><i class="fa fa-arrows-alt"></i></a>
								</div>
							</div>
						</div>
						<div class="project-post buildings isolation snd-size">
							<img src="<?php echo base_url(); ?>assets/sintec/upload/projects/11.jpg" alt="">
							<div class="hover-box">
								<div class="inner-hover">
									<h2><a href="single-project.html">Classic House</a></h2>
									<span>bulding, house</span>
									<a href="single-project.html" class="link"><i class="fa fa-link"></i></a>
									<a href="<?php echo base_url(); ?>assets/sintec/upload/projects/11.jpg" class="zoom"><i class="fa fa-arrows-alt"></i></a>
								</div>
							</div>
						</div>
						<div class="project-post interior default-size">
							<img src="<?php echo base_url(); ?>assets/sintec/upload/projects/7.jpg" alt="">
							<div class="hover-box">
								<div class="inner-hover">
									<h2><a href="single-project.html">Beatiful House</a></h2>
									<span>bulding, house</span>
									<a href="single-project.html" class="link"><i class="fa fa-link"></i></a>
									<a href="<?php echo base_url(); ?>assets/sintec/upload/projects/2.jpg" class="zoom"><i class="fa fa-arrows-alt"></i></a>
								</div>
							</div>
						</div>
						<div class="project-post buildings">
							<img src="<?php echo base_url(); ?>assets/sintec/upload/projects/4.jpg" alt="">
							<div class="hover-box">
								<div class="inner-hover">
									<h2><a href="single-project.html">Modern and trending house</a></h2>
									<span>bulding, house</span>
									<a href="single-project.html" class="link"><i class="fa fa-link"></i></a>
									<a href="<?php echo base_url(); ?>assets/sintec/upload/projects/4.jpg" class="zoom"><i class="fa fa-arrows-alt"></i></a>
								</div>
							</div>
						</div>
						<div class="project-post interior isolation">
							<img src="<?php echo base_url(); ?>assets/sintec/upload/projects/10.jpg" alt="">
							<div class="hover-box">
								<div class="inner-hover">
									<h2><a href="single-project.html">Afarist Building</a></h2>
									<span>bulding, house</span>
									<a href="single-project.html" class="link"><i class="fa fa-link"></i></a>
									<a href="<?php echo base_url(); ?>assets/sintec/upload/projects/5.jpg" class="zoom"><i class="fa fa-arrows-alt"></i></a>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- End projects -->

				<!-- about section 
					================================================== -->
				<section class="about-section">
					<div class="container">
						<div class="row">
							<div class="col-md-3">
								<div class="default-article">
									<h1>Here Everythink about us</h1>
									<span>About us</span>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
									<a href="about.html">Learn More</a>
								</div>
							</div>
							<div class="col-md-5">
								<!-- Vimeo -->
								<iframe class="videoembed" src="http://player.vimeo.com/video/23031622?title=0&amp;byline=0&amp;portrait=0" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe>
								<!-- End Vimeo -->
							</div>
							<div class="col-md-4">
								<img src="upload/others/4.jpg" alt="">
							</div>
						</div>
					</div>
				</section>
				<!-- End about section -->

				<!-- about section 
					================================================== -->
				<section class="about-alternative-section">
					<div class="container">
						<div class="row">
							<div class="col-md-4">
								<!-- Nav tabs -->
								<div class="tab-posts-box">
									<ul class="nav nav-tabs" id="myTab">
										<li class="active">
											<a href="#option1" data-toggle="tab">Tire</a>
										</li>
										<li>
											<a href="#option2" data-toggle="tab">Change Oil</a>
										</li>
										<li>
											<a href="#option3" data-toggle="tab">Repair</a>
										</li>
									</ul>

									<div class="tab-content">
										<div class="tab-pane active" id="option1">
											<img src="upload/others/1.jpg" alt="">

											<p>quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo. ullamco laboris nisi ut aliquip ex ea commodo.</p>
										</div>
										<div class="tab-pane" id="option2">
											<img src="upload/others/2.jpg" alt="">

											<p>quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo. ullamco laboris nisi ut aliquip ex ea commodo.</p>
										</div>
										<div class="tab-pane" id="option3">
											<img src="upload/others/3.jpg" alt="">

											<p>quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo. ullamco laboris nisi ut aliquip ex ea commodo.</p>

										</div>
									</div>
								</div>
							</div>

							<div class="col-md-4">
								<div class="accordion-box">
									<div class="accord-elem active">
										<div class="accord-title">
											<a class="accord-link" href="#"></a>
											<h2>Vivamus vestibulum nulla nec ante.</h2>
										</div>
										<div class="accord-content">
											<p>Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat. Praesent dapibus, neque id cursus faucibus.</p>
										</div>
									</div>

									<div class="accord-elem">
										<div class="accord-title">
											<a class="accord-link" href="#"></a>
											<h2>Morbi in sem quis dui placerat </h2>
										</div>
										<div class="accord-content">
											<p>Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat. Praesent dapibus, neque id cursus faucibus.</p>
										</div>
									</div>

									<div class="accord-elem">
										<div class="accord-title">
											<a class="accord-link" href="#"></a>
											<h2>Lorem ipsum dolor sit amet. </h2>
										</div>
										<div class="accord-content">
											<p>Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat. Praesent dapibus, neque id cursus faucibus.</p>
										</div>
									</div>

									<div class="accord-elem">
										<div class="accord-title">
											<a class="accord-link" href="#"></a>
											<h2>Lorem ipsum dolor sit amet. </h2>
										</div>
										<div class="accord-content">
											<p>Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat. Praesent dapibus, neque id cursus faucibus.</p>
										</div>
									</div>
								</div>
							</div>

							<div class="col-md-4">
								<div class="second-article">
									<img src="upload/others/3a.jpg" alt="">
									<h2>Who we are</h2>
									<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
									<p>Excepteur sint occaecat cupidatat non proident, sunt</p>
								</div>
							</div>

						</div>
					</div>
				</section>
				<!-- End about section -->

				<!-- news-section 
					================================================== -->
				<section class="news-section">
					<div class="container">
						<div class="title-section alt-title">
							<h1>Pengurus Sintec Tahun Ajaran 2015/2016</h1>
						</div>
						<div class="news-box">
							<div class="arrow-box">
								<a href="#" class="prev"><i class="fa fa-angle-left"></i></a>
								<a href="#" class="next"><i class="fa fa-angle-right"></i></a>
							</div>
							
							<div id="owl-demo" class="owl-carousel">
								<?php 
									foreach ($pengurus as $q_pengurus) {
										echo '
											<div class="item news-post">
												<img src="'.base_url().'images/pengurus/'.$q_pengurus->foto.'" alt="">
												<center><h2><a href="#">'.$q_pengurus->nama_pengurus.'</a></h2></center>
												<ul class="post-tags">
													<li> <strong>Jabatan : </strong>'.$q_pengurus->jabatan_pengurus.'</li>
													<li><strong>Kelas : </strong>'.$q_pengurus->kelas_pengurus.'</li>
													<li><strong>SMAN 1 Puri Mojokerto</strong></li>
												</ul>
											</div>
										';
									}
								?>
								
							</div>
						</div>

					</div>
				</section>
				<!-- End news section -->

				<!-- testimonial-section 
					================================================== -->
				<section class="testimonial-section">
					<div class="container">

						<div class="testimonial-box">
							<ul class="bxslider">
								<li>
									<h2>Steve Ballmer</h2>
									<span>CEO Microsoft (2000-2014)</span>
									<p>The number one benefit of information technology is thatit empowers people to do what they want to do. It lets people be productive. it lets people learn thingsthey didn't think they could learn before,and so in a sense it is all about potential</p>
								</li>
								<li>
									<h2>Bill Gates</h2>
									<span>Founder Microsoft</span>
									<p>Technology is just a tool. In terms of getting the kids working together and motivating them, the teacher is the most important</p>
								</li>
								<li>
									<h2>Steve Jobs</h2>
									<span>Founder Apple</span>
									<p>Technology is nothing.what's importantis that youhave a faith in people, that they're basically good and smart, and if you give them tools, they'll do wonderful things with them</p>
								</li>
							</ul>
						</div>

					</div>
				</section>
				<!-- End testimonial section -->

				<!-- quote-section 
					================================================== -->
				<section class="quote-section">
					<div class="container">
						<div class="title-section alt-title">
							<h1>Do you plan to build or renovate ?</h1>
						</div>
						<form id="quote-form">
							<div class="row">
								<div class="col-md-4">
									<input name="first-name" id="first-name" type="text" placeholder="First Name">
									<input name="last-name" id="last-name" type="text" placeholder="Last Name">
									<input name="mail" id="mail" type="text" placeholder="Email">
									<input name="tel-number" id="tel-number" type="text" placeholder="Phone">
								</div>
								<div class="col-md-4">
									<select name="type">
										<option>Car type</option>
										<option>Audi</option>
										<option>Citroen</option>
										<option>Renault</option>
										<option>Mercedes</option>
										<option>Ferrari</option>
									</select>
									<select name="budget">
										<option>Budget</option>
										<option value="$2000 - $5000">$200 - $500</option>
										<option value="$5000 - $10k">$500 - $1000</option>
										<option value="$10k - $20k">$1000 - $1500</option>
										<option value="$20k - $40k">$1500 - $2000</option>
										<option value="$40k +">$2000 + </option>
									</select>
									<select name="your-area">
										<option>type service</option>
										<option value="motor">Motor</option>
										<option value="oil">Oil</option>
										<option value="tire">tire replacement</option>
										<option value="paint">paint</option>
										<option value="other">other problem</option>
									</select>
									<select name="time">
										<option>Time to be done</option>
										<option value="1 Day">1 Day</option>
										<option value="2 day">2 day</option>
										<option value="5 days">5 days</option>
										<option value="1 week">1 week</option>
										<option value="this month">this month </option>
									</select>
								</div>
								<div class="col-md-4">
									<textarea name="your-comment" id="your-comment" placeholder="Describe your car problem..."></textarea>
									<input type="submit" id="submit-quote" value="Send">
								</div>
							</div>
							<div id="quote-msg" class="message"></div>
						</form>
					</div>
				</section>
				<!-- End quote section -->

				<!-- clients-section 
					================================================== -->
				