/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
config.filebrowserBrowseUrl = '<?php echo base_url(); ?>assets/global/plugins/kcfinder/browse.php?type=files';
config.filebrowserImageBrowseUrl = '<?php echo base_url(); ?>assets/global/plugins/kcfinder/browse.php?type=images';
config.filebrowserFlashBrowseUrl = '<?php echo base_url(); ?>assets/global/plugins/kcfinder/browse.php?type=flash';
config.filebrowserUploadUrl = '<?php echo base_url(); ?>assets/global/plugins/kcfinder/upload.php?type=files';
config.filebrowserImageUploadUrl = '<?php echo base_url(); ?>assets/global/plugins/kcfinder/upload.php?type=images';
config.filebrowserFlashUploadUrl = '<?php echo base_url(); ?>assets/global/plugins/kcfinder/upload.php?type=flash';
};
