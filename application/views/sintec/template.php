<!doctype html>


<html lang="en" class="no-js">
<head>
	<title>Sintec</title>

	<meta charset="utf-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<link href='http://fonts.googleapis.com/css?family=Roboto:400,400italic,500,500italic,700,900,300' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Dancing+Script:400,700' rel='stylesheet' type='text/css'>
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/sintec/css/bootstrap.min.css" media="screen">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/sintec/css/jquery.bxslider.css" media="screen">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/sintec/css/magnific-popup.css" media="screen">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/sintec/css/owl.carousel.css" media="screen">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/sintec/css/owl.theme.css" media="screen">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/sintec/css/font-awesome.css" media="screen">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/sintec/css/animate.css" media="screen">
    <!-- REVOLUTION BANNER CSS SETTINGS -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/sintec/css/settings.css" media="screen"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/sintec/css/style.css" media="screen">


	<script type="text/javascript" src="<?php echo base_url(); ?>assets/sintec/js/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/sintec/js/jquery.migrate.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/sintec/js/jquery.bxslider.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/sintec/js/jquery.magnific-popup.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/sintec/js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/sintec/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/sintec/js/jquery.imagesloaded.min.js"></script>
  	<script type="text/javascript" src="<?php echo base_url(); ?>assets/sintec/js/jquery.isotope.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/sintec/js/retina-1.1.0.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/sintec/js/plugins-scroll.js"></script>
    <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/sintec/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/sintec/js/jquery.themepunch.revolution.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/sintec/js/script.js"></script>

	<?php
		if($this->uri->segment(2) == 'artikel'){
	?>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/sintec/css/bootstrap.min.css" media="screen">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/sintec/css/font-awesome.css" media="screen">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/sintec/css/flexslider.css" media="screen">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/sintec/css/style.css" media="screen">
	<?php
		}
	?>

</head>
<body>

	<!-- Container -->
	<div id="container">
		<!-- Header
		    ================================================== -->
		<header class="clearfix">
			<div class="logo">
				<a href="index.html"><img src="<?php echo base_url(); ?>assets/sintec/images/logo2.png" alt=""></a>
			</div>

			<a class="elemadded responsive-link" href="#">Menu</a>

			<nav class="nav-menu">
				<ul class="menu-list">
					<li class="drop"><a class="active" href="<?php echo base_url(); ?>main">Home</a>
					</li>
					<li class="drop"><a href="<?php echo base_url(); ?>main/portofolio">Portofolio</a>
						<ul class="dropdown">
							<li><a href="projects-2col.html">Projects 2 Colums</a></li>
							<li><a href="projects-3col.html">Projects 3 Colums</a></li>
							<li><a href="projects-4col.html">Projects 4 Colums</a></li>
							<li><a href="projects-fullwidth.html">Projects Fullwidth</a></li>
							<li><a href="projects-masonry.html">Projects Masonry</a></li>
							<li><a href="single-project.html">Single Project</a></li>
						</ul>
					</li>
					<li class="drop"><a href="<?php echo base_url(); ?>main/tutorial">Tutorial</a>
						<ul class="dropdown">
							<li><a href="repair.html">Corel Draw</a></li>
							<li><a href="inspect.html">Adobe Photoshop</a></li>
							<li><a href="oil-change.html">After Effect</a></li>
							<li><a href="balancing.html">Pemrograman Pascal</a></li>
							<li><a href="tire-replacement.html">Pemrograman Web</a></li>
						</ul>
					</li>
					<li class="drop"><a href="<?php echo base_url(); ?>main/artikel">Blog</a>
						<ul class="dropdown">
							<li><a href="blog.html">Blog Default</a></li>
							<li><a href="blog-2col.html">Blog 2 Colums</a></li>
							<li><a href="blog-3col.html">Blog 3 Colums</a></li>
							<li><a href="blog-4col.html">Blog 4 Colums</a></li>
							<li><a href="single-post.html">Single Post</a></li>
						</ul>
					</li>
					<li class="drop"><a href="contact.html">Contact</a>
						<ul class="dropdown">
							<li><a href="contact.html">Contact 1</a></li>
							<li><a href="contact2.html">Contact 2</a></li>
						</ul>
					</li>
				</ul>
			</nav>
			<div class="working-hours">
				<h2>Jadwal Kegiatan</h2>
				<p><span>Hari</span> Rabu - Kamis </p>
				<p><span>Jam</span> 15:00 - 16:30</p>
				<p><span>Tempat</span> Lab Komputer </p>
			</div>
		</header>
		<!-- End Header -->

		<!-- content
			================================================== -->
		<div id="content">
			<div class="inner-content">
				<div class="top-line">
					<div class="container">
						<div class="row">
							<div class="col-md-6">
								<p>
									<span><i class="fa fa-phone"></i>08585147289</span>
									<span><i class="fa fa-envelope-o"></i>sintec-castle@gmail.com</span>
									<a href="<?php echo base_url(); ?>auth"><i class="fa fa-user"></i>Log In</a>
								</p>
							</div>
							<div class="col-md-6">
								<ul class="social-icons">
									<li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
									<li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
									<li><a class="rss" href="#"><i class="fa fa-rss"></i></a></li>
									<li><a class="google" href="#"><i class="fa fa-google-plus"></i></a></li>
									<li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
									<li><a class="pinterest" href="#"><i class="fa fa-pinterest"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>

				<?php echo $_content; ?>

				<?php echo $_footer; ?>
			</div>
		</div>
		<!-- End Content -->
	</div>
	<!-- End Container -->
	<?php
	$url = $this->uri->segment(2);
	switch ($url) {
		case '':
		?>
			<!-- Revolution slider -->
			<script type="text/javascript">

				jQuery(document).ready(function() {

					jQuery('.tp-banner').show().revolution(
					{
						dottedOverlay:"none",
						delay:10000,
						startwidth:1140,
						startheight:550,
						hideThumbs:200,

						thumbWidth:100,
						thumbHeight:50,
						thumbAmount:5,

						navigationType:"bullet",

						touchenabled:"on",
						onHoverStop:"off",

						swipe_velocity: 0.7,
						swipe_min_touches: 1,
						swipe_max_touches: 1,
						drag_block_vertical: false,

												parallax:"mouse",
						parallaxBgFreeze:"on",
						parallaxLevels:[7,4,3,2,5,4,3,2,1,0],

						keyboardNavigation:"off",

						navigationHAlign:"center",
						navigationVAlign:"bottom",
						navigationHOffset:0,
						navigationVOffset:"center",

						shadow:0,

						spinner:"spinner4",

						stopLoop:"off",
						stopAfterLoops:-1,
						stopAtSlide:-1,

						shuffle:"off",

						autoHeight:"off",
						forceFullWidth:"off",



						hideThumbsOnMobile:"off",
						hideNavDelayOnMobile:1500,
						hideBulletsOnMobile:"off",
						hideArrowsOnMobile:"off",
						hideThumbsUnderResolution:0,

						hideSliderAtLimit:0,
						hideCaptionAtLimit:0,
						hideAllCaptionAtLilmit:0,
						startWithSlide:0,
						fullScreenOffsetContainer: ".header"
					});

				});	//ready

				//isotope
				jQuery(document).ready(function() {
					var $container = $('.iso-call');
					// init
					$container.isotope({
						// options
						itemSelector: '.services-project, .project-post',
						masonry: {
						    columnWidth: '.default-size'
						}
					});
				});	//ready
			</script>
		<?php
			break;
		case 'artikel':
		?>
			<script type="text/javascript" src="<?php echo base_url(); ?>assets/sintec/js/jquery.min.js"></script>
			<script type="text/javascript" src="<?php echo base_url(); ?>assets/sintec/js/jquery.migrate.js"></script>
			<script type="text/javascript" src="<?php echo base_url(); ?>assets/sintec/js/bootstrap.min.js"></script>
			<script type="text/javascript" src="<?php echo base_url(); ?>assets/sintec/js/jquery.imagesloaded.min.js"></script>
			<script type="text/javascript" src="<?php echo base_url(); ?>assets/sintec/js/jquery.flexslider.js"></script>
			<script type="text/javascript" src="<?php echo base_url(); ?>assets/sintec/js/retina-1.1.0.min.js"></script>
			<script type="text/javascript" src="<?php echo base_url(); ?>assets/sintec/js/plugins-scroll.js"></script>
			<script type="text/javascript" src="<?php echo base_url(); ?>assets/sintec/js/script.js"></script>
		<?php
			break;
		default:
			# code...
			break;
	}
	?>

</body>
</html>
